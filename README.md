# Todo Application with React
Bu uygulama react ile unit ve e2e test yapılmış bir todo örneğidir. Proje aynı zamanda bir golang ile yazılmış bir back-end'e bağımlıdır.

Geçici olarak bu adresten yayınlanıyor: [http://157.230.121.51/](http://157.230.121.51/)

![image1](./public/gif.gif)

## Kullanılan teknolojiler

- React
- Jest
- Puppeteer

## Özellikleri

ToDo eklemek/silmek/tamamlamak

## Kurmak için yapılanlar

Gereklilikler;

- NodeJs environment
- Docker environment

Kurmak için:

```
npm install
docker build -t abdurrahmanekr/todo-app .
docker run abdurrahmanekr/todo-app
```

Back-end projesi eğer ayaktaysa bağlantı kurup kullanılabillir. Back-end projesinin kurulum adımları için: [todo-app-server](https://gitlab.com/abdurrahmanekr/todo-app-server)


## Deployment
Proje Docker Swarm çalışan bir sunucuya deploy ediliyor. Bu projedeki docker-compose.yml dosyası o sunucunun içinde ve aşağıdaki komutu çalıştırmak uygulamaların ayağa kalkması için yeterli:

```
docker stack deploy --compose-file docker-compose.yml todo-app
```