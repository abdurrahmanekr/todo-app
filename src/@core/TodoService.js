export const URL = `http://${window?.location?.hostname || 'localhost'}:8080`;

class TodoService {
  getAll() {
    return fetch(`${URL}/todos`)
    .then(res => res.json());
  }

  add(content) {
    return fetch(`${URL}/todos`, {
      method: 'POST',
      body: JSON.stringify({ content }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(res => res.json());
  }

  delete(id) {
    return fetch(`${URL}/todos/${id}`, {
      method: 'DELETE',
    })
    .then(res => res.json());
  }

  setCompleted(id, complete) {
    return fetch(`${URL}/todos/${id}/${complete ? 'completed' : 'uncompleted'}`, {
      method: 'PUT',
    })
    .then(res => res.json());
  }
}

export default new TodoService();
