import '@testing-library/jest-dom'
import * as React from 'react'
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { rest } from 'msw'
import { setupServer } from 'msw/node'

import { URL } from '../../@core/TodoService';
import Home from '../../pages/Home';

const fakeAllTodos = [];
const fakeTodo = { id: 1, content: 'Buy some milk', timestamp: new Date().toISOString(), completed: false };

const server = setupServer(
  rest.get(`${URL}/todos`, (req, res, ctx) => {
    return res(ctx.json(fakeAllTodos))
  }),
  rest.post(`${URL}/todos`, (req, res, ctx) => {
    fakeAllTodos.push(fakeTodo)
    return res(ctx.json(fakeTodo))
  }),
)

beforeAll(() => server.listen())
afterEach(() => {
  server.resetHandlers()
})
afterAll(() => server.close())

describe('ToDo Testing', () => {
  test('when I wrote <todo> to <text-box> and Then I should see <todo> item in todo', async () => {
    render(<Home />);
    fireEvent.click(screen.getByTestId('todo-add-open'));
    fireEvent.change(screen.getByTestId('todo-input'), {
      target: { value: fakeTodo.content },
    })
    fireEvent.click(screen.getByTestId('todo-add-button'))

    await waitFor(() => {
      expect(screen.getByText('Buy some milk')).toBeInTheDocument();
    })
  });
})
