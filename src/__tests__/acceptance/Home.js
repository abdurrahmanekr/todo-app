import puppeteer from 'puppeteer';
import assert from 'assert';

jest.setTimeout(50 * 1000)
test('AcceptanceTest', async () => {
  const browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage'
    ]
  });
  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080
  });

  // Given Empty ToDo list
  // When I write "buy some milk" to <text-box> and
  // Then I should see "buy some milk" item in ToDo
  await page.goto("http://157.230.121.51/")
  await page.click("div[data-testid='todo-add-open']")
  await page.type("input[data-testid='todo-input']", "buy some milk")
  await page.click("div[data-testid='todo-add-button']")
  await page.waitForSelector('.todo-item-timestamp')
  const todos = await page.$eval(".todo-list", div => div.innerText);
  await assert.ok(todos.includes("buy some milk"))

  browser.close();
});
