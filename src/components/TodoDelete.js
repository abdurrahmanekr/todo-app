import React from 'react';
import { MdDelete } from 'react-icons/md';

function TodoDelete(props) {
  return (
    <div className="todo-delete" {...props}>
      <MdDelete/>
    </div>
  );
}

export default TodoDelete
