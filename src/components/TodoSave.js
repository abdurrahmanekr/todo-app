import React from 'react';
import { FaSave } from 'react-icons/fa';

function TodoSave(props) {
  return (
    <div data-testid="todo-add-button" className="todo-add pr-0" {...props}>
      <FaSave/>
    </div>
  );
}

export default TodoSave
