import React from 'react';
import { FaPlus } from 'react-icons/fa';

function TodoAdd(props) {
  return (
    <div data-testid="todo-add-open" className="todo-add" {...props}>
      <FaPlus/>
    </div>
  );
}

export default TodoAdd
