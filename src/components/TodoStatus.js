import React from 'react';
import { FaCheckCircle } from 'react-icons/fa';
import classnames from 'classnames';

function TodoStatus(props) {
  return (
    <div
      onClick={props.onClick}
      className={classnames('todo-status', {
        complated: props.status,
      })}>
      <FaCheckCircle/>
    </div>
  );
}

export default TodoStatus
