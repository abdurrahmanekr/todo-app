import { useState, useEffect, useRef } from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Spinner,
  Input,
} from 'reactstrap';

import classnames from 'classnames';

import TodoService from '../@core/TodoService';

import TodoStatus from '../components/TodoStatus';
import TodoDelete from '../components/TodoDelete';
import TodoAdd from '../components/TodoAdd';
import TodoSave from '../components/TodoSave';

function AddTodo() {
  const [loading, setLoading] = useState(true);
  const [adding, setAdding] = useState(false);
  const [addLoading, setAddLoading] = useState(false);
  const [data, setData] = useState([]);

  const todoAddRef = useRef(null);

  useEffect(() => {
    getAll();
  }, [])

  const getAll = () => {
    setLoading(true)

    return TodoService.getAll()
    .then(setData)
    .catch(err => console.log(err))
    .then(() => setLoading(false))
  };

  const addTodo = () => {
    if (addLoading)
      return;

    setAddLoading(true);
    const content = todoAddRef && (todoAddRef.current.value || '').trim();
    if (!content) {
      alert('Boş bırakılamaz');
      setAddLoading(false);
      return;
    }

    TodoService.add(content)
    .then(getAll)
    .catch(err => console.log(err))
    .then(() => {
      setAdding(false)
      setAddLoading(false)
    })
  }

  const deleteTodo = (id) => {
    TodoService.delete(id)
    .then(getAll)
    .catch(err => console.log(err))
  }

  const toggleTodo = (id, completed) => {
    TodoService.setCompleted(id, completed)
    .then(getAll)
    .catch(err => console.log(err))
  }

  return (
    <Container>
      <Row className='mt-5'>
        <Col>
          <Card>
            <CardBody>
              <CardTitle>
                <div className="todo-header">
                  <h5 className='title'>
                    Todolarım
                    {
                      loading &&
                      <div className="todo-loading">
                        <Spinner
                          color="primary"/>
                      </div>
                    }
                  </h5>
                  <TodoAdd
                    onClick={() => setAdding(true)}/>
                </div>
              </CardTitle>

              {
                !loading &&
                data.length < 1 &&
                !adding &&
                <span className='text-center'>Bir ToDo bulunamadı</span>
              }

              {
                adding &&
                <div className="todo-list-item">

                  <Input
                    innerRef={todoAddRef}
                    type="text"
                    data-testid="todo-input"
                    placeholder="Ne yapmak istiyorsunuz?"
                    disabled={addLoading}
                    autoFocus={true}
                    className="todo-item-content"/>

                  <TodoSave
                    onClick={addTodo}/>

                  <TodoDelete
                    onClick={() => !addLoading && setAdding(false)}/>
                </div>
              }

              <div className='todo-list'>
                {
                  (!loading || data.length > 0) &&
                  data.map((x, i) => (
                    <div
                      key={i}
                      className={classnames("todo-list-item", {
                        'completed': x.completed,
                      })}>
                      <TodoStatus
                        onClick={() => toggleTodo(x.id, !x.completed)}
                        status={x.completed}/>

                      <div className="todo-item-content">
                        <div className="todo-item-timestamp">
                          {x.timestamp}
                        </div>
                        {x.content}
                      </div>

                      <TodoDelete
                        onClick={() => deleteTodo(x.id)}/>
                    </div>
                  ))
                }
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default AddTodo;
